The JavaScript program reads both Locatios.csv and Employees.csv files from a specified Azure storage account
and post them to the existing endpoints post-csv-locations and post-csv-individuals for further csv entries processing.

The program is currently located at the Azure VM ubuntubcvm0(20.116.82.88) in /home/crediadmin/data/azure/access-csv-azure-storage. 

Usage:
1. Run 'node upload-file.js' 
   - to upload the test Locatios.csv and Employees.csv to the Azure storage as specified in .env.

2. Run 'node get-post-file.js' 
   - to reads the csv files from Azure storage and post them to the post-csv-locations
     and post-csv-individuals endpoints for csv processing.

3. Update the test Locatios.csv and Employees.csv and repeat 1 & 2 above.

4. If reading the csv file in storage is scheduled, run "node scheduler.js" so the tasks will 
   run at specified times every day.