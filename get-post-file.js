/*
 * Get csv files from Azure storage and post them to post-csv endpoints. 
 */
'use strict'

const { BlobServiceClient, StorageSharedKeyCredential } = require("@azure/storage-blob");
const fetch = require('node-fetch');
require("dotenv").config();

async function main() {
  // Enter your storage account name and shared key
  const account = process.env.account_name || "";
  const accountKey = process.env.account_key || ""; 
  
  // Use StorageSharedKeyCredential with storage account and account key
  // StorageSharedKeyCredential is only avaiable in Node.js runtime, not in browsers
  const sharedKeyCredential = new StorageSharedKeyCredential(account, accountKey);

  const blobServiceClient = new BlobServiceClient(  
    `https://${account}.blob.core.windows.net`,
    sharedKeyCredential
  );  

  // Set container client
  const containerName = "locations-employees";
  const containerClient = blobServiceClient.getContainerClient(containerName);

  // Post Locations csv
  let blobName = "Locations.csv";
  let url = process.env.base_url + '/' + process.env.build_stage + '/post-csv-locations/' + process.env.organization_id; 
  await postCsv(containerClient, blobName, url);

  // Post Employees csv
  blobName = "Employees.csv";
  url =  process.env.base_url + '/' + process.env.build_stage + '/post-csv-individuals/' + process.env.organization_id;
  await postCsv(containerClient, blobName, url);
}

async function postCsv(containerClient, blobName, url) {
  const blockBlobClient = containerClient.getBlockBlobClient(blobName);

  // Get blob content from position 0 to the end
  // In Node.js, get downloaded data by accessing downloadBlockBlobResponse.readableStreamBody
  const downloadBlockBlobResponse = await blockBlobClient.download(0);
  const blobBody = await streamToBuffer(downloadBlockBlobResponse.readableStreamBody);
  
  fetch(url, { 
    headers: { "Content-Type": "text/csv"}, 
    method: "POST",
    body: blobBody})
    .then(() => {
      // To-Do: Delete the csv files from the storage
    })
    .catch((err) => console.log("Error postCsv", err.message));  
}

// A helper method used to read a Node.js readable stream into a Buffer
async function streamToBuffer(readableStream) {
  return new Promise((resolve, reject) => {
    const chunks = [];
    readableStream.on("data", (data) => {
      chunks.push(data instanceof Buffer ? data : Buffer.from(data));
    });
    readableStream.on("end", () => {
      resolve(Buffer.concat(chunks));
    });
    readableStream.on("error", reject);
  });
}

main().catch((err) => {
  console.error("Error get-post-file:", err.message);
});