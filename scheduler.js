/*
 * The scheduler to get csv files from Azure storage and post them to post-csv endpoints
 * at specified different times every day. 
 */
'use strict'

const cron = require("node-cron");
const { exec } = require("child_process");

// Schedule and run the task at 10:30 UTC every day
const task1 = cron.schedule("30 10 * * *", () => {
    executeTask();
});
task1.start();

// Schedule and run the task to run 16:30 UTC every day
const task2 = cron.schedule("30 16 * * *", () => {
    executeTask();
});
task2.start();

function executeTask() {
    exec("node ./get-post-blob.js", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }       
        console.log(`stdout: ${stdout}: executed get-post-blob.js at ${new Date().toUTCString()} `);
    });
}