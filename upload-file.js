/*
 * Upload files to Azure storage.
 */

'use strict'

const { BlobServiceClient, StorageSharedKeyCredential } = require("@azure/storage-blob");
require("dotenv").config();

async function main() {
  // Enter your storage account name and shared key
  const account = process.env.account_name || "";
  const accountKey = process.env.account_key || "";  
  
  // Use StorageSharedKeyCredential with storage account and account key
  // StorageSharedKeyCredential is only avaiable in Node.js runtime, not in browsers
  const sharedKeyCredential = new StorageSharedKeyCredential(account, accountKey);

  const blobServiceClient = new BlobServiceClient(    
    `https://${account}.blob.core.windows.net`,
    sharedKeyCredential
  ); 

  // Get container
  const containerName = "locations-employees";
  const containerClient = blobServiceClient.getContainerClient(containerName);

  if (!(await containerClient.exists())) {
    const createContainerResponse = await containerClient.create();
    console.log(`Create container ${containerName} successfully`, createContainerResponse.requestId);   
  }

  // Upload file for locations
  let blockName = "Locations.csv";
  let blockBlobClient = containerClient.getBlockBlobClient(blockName);
  let localFilePath = "Locations.csv";
  await uploadFile(containerClient, blockName, localFilePath)

  // Upload file for employees
  blockName = "Employees.csv";
  blockBlobClient = containerClient.getBlockBlobClient(blockName);
  localFilePath = "Employees.csv";
  await uploadFile(containerClient, blockName, localFilePath);
}

async function uploadFile(containerClient, blockName, localFilePath) {  
  const blockBlobClient = containerClient.getBlockBlobClient(blockName);

  try {
    await blockBlobClient.uploadFile(localFilePath, {
      blockSize: 4 * 1024 * 1024, // 4MB block size
      concurrency: 20, // 20 concurrency
      onProgress: (ev) => console.log(ev)
    });
    console.log(`uploadFile ${localFilePath} succeeds`);
  } catch (err) {
    console.log(
      `uploadFile failed ${err.message}}`
    );
  }
}

main().catch((err) => {
  console.error("Error uploaing file:", err.message);
});